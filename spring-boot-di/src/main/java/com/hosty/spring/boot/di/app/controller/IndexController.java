package com.hosty.spring.boot.di.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.hosty.spring.boot.di.app.models.service.IServicio;

@Controller
public class IndexController {

	@Autowired
	@Qualifier("MiServicio1")
	private IServicio miServicio;
	
	@GetMapping({"","/","/Home"})
	public String index(Model model) {
		model.addAttribute("objeto", miServicio.operacion());
		return "index";
	}
	
	
}
