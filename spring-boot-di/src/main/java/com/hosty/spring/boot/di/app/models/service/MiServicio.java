package com.hosty.spring.boot.di.app.models.service;

import org.springframework.stereotype.Service;

//@Service("MiServicio1")
public class MiServicio implements IServicio{
	
	public String operacion() {
		return "Ejecutando algun proceso importante...";
	}

}
