package com.hosty.spring.boot.di.app;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hosty.spring.boot.di.app.models.domain.ItemFactura;
import com.hosty.spring.boot.di.app.models.domain.Producto;
import com.hosty.spring.boot.di.app.models.service.IServicio;
import com.hosty.spring.boot.di.app.models.service.MiServicio;
import com.hosty.spring.boot.di.app.models.service.MiServicio2;

@Configuration
public class AppConfig {
	
	@Bean("MiServicio1")
	public IServicio registrarMiServicio() {
		return new MiServicio();
	}
	
	@Bean("MiServicio2")
	public IServicio registrarMiServicio2() {
		return new MiServicio2();
	}
	
	@Bean("itemsFactura")
	public List<ItemFactura> registrarItems(){
		Producto pr = new Producto("Camara sony", 100);
		Producto pr2 = new Producto("Biclicleta bianchi aro 26", 1000);
		Producto pr3 = new Producto("Play 67", 5000);
		return Arrays.asList(new ItemFactura(pr, 1), new ItemFactura(pr2, 5), new ItemFactura(pr3, 2));
	} 
	
	@Bean("itemsFacturaOficina")
	public List<ItemFactura> registrarItemsOficina(){
		Producto pr = new Producto("Monitor LG", 500);
		Producto pr2 = new Producto("Notebook Asus", 1500);
		Producto pr3 = new Producto("Play 5", 2000);
		Producto pr4 = new Producto("Impresora HP multifuncional", 300);
		return Arrays.asList(new ItemFactura(pr, 7 ), new ItemFactura(pr2, 2), new ItemFactura(pr3, 3), new ItemFactura(pr4, 5));
	} 
}
