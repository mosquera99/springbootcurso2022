package com.hosty.springboot.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hosty.springboot.app.models.entity.Cliente;
import com.hosty.springboot.app.models.service.IClienteService;

@Controller
public class ClienteController {
	
	@Autowired
//	@Qualifier("clienteDaoJPA")
	private IClienteService clienteService;
	
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String findAll(Model md){
		md.addAttribute("title", "Listado de clientes");
		md.addAttribute("clientes", clienteService.findAll());
		return "listar";
	}
	
	@RequestMapping(value="/form/{id}")
	public String findOne(@PathVariable(value="id") Long id, Model md) {
		Cliente cliente = null;
		if (id>0) {
			cliente=clienteService.findOne(id);
		}else {
			return "redirect:/listar";
		}
		md.addAttribute("title", "Editar clientes");
		md.addAttribute("cliente", cliente);
		return "form";
	}
	
	@RequestMapping(value = "/form")
	public String crear(Model md){
		Cliente cliente = new Cliente();
		md.addAttribute("title", "Formulario de clientes");
		md.addAttribute("cliente", cliente);
		return "form";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String save(@Valid Cliente cliente, BindingResult reulst, Model md){
		if (reulst.hasErrors()) {
			md.addAttribute("title", "Formulario cliente");
			return "form";	
		}
		clienteService.save(cliente);
		return "redirect:/listar";
	}
	
	@RequestMapping(value="/eliminar/{id}")
	public String delete (@PathVariable Long id){
		if (id>0) {
			clienteService.delete(id);	
		}
		return "redirect:/listar";
	}
	
	 
	
}
