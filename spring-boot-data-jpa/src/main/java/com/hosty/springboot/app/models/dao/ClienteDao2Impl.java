package com.hosty.springboot.app.models.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.hosty.springboot.app.models.entity.Cliente;

@Repository(/*"clienteDaoJPA"*/)
public class ClienteDao2Impl implements IClienteDao2{
	
	@PersistenceContext
	EntityManager em;
	
	@SuppressWarnings("unchecked")
//	@Override
	public List<Cliente> findAll() {
		return em.createQuery("from Cliente").getResultList();
	}
	
//	@Override
	public Cliente findOne(Long id) {
		return em.find(Cliente.class, id);
	}

//	@Override
	public void save(Cliente cliente) {
		System.err.println("Cliente a guardar: "+cliente.toString());
		if (null!=cliente.getId() && cliente.getId() > 0) {
			em.merge(cliente);
		} else {
			em.persist(cliente);
		}
		
	}

//	@Override
	public void delete(Long id) {
		em.remove(findOne(id));
	}
	
}
