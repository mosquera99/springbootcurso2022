package com.hosty.springboot.app.models.dao;

import java.util.List;

import com.hosty.springboot.app.models.entity.Cliente;

public interface IClienteDao2 {
	
	List<Cliente> findAll();
	
	void save(Cliente cliente);
	
	Cliente findOne(Long id);
	
	void delete(Long id);
}
