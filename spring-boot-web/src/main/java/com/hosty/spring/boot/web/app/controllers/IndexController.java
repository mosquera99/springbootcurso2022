package com.hosty.spring.boot.web.app.controllers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hosty.spring.boot.web.app.models.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {
	
	@Value("${texto.indexcontroller.index.title}")
	private String textoIndex;
	@Value("${texto.indexcontroller.perfil.title}")
	private String textoPerfil;
	@Value("${texto.indexcontroller.listar.title}")
	private String textoListar;
	
	@GetMapping({"/index","/","home"})
	public String index(Model model) {
		model.addAttribute("titulo", textoIndex);
		return "index";
	}
	
	@GetMapping("/perfil")
	public String perfil(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("Luis");
		usuario.setApellido("Suarez Mosquera!!!!!!!");
		usuario.setEmail("luchitomosquera@mail.com");
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", textoPerfil.concat(usuario.getNombre()));
		return "perfil";
	}
	
	@GetMapping("/listar")
	public String listar(Model model) {
		/*Ways to fill out a list.
		List<Usuario> usuarios = Arrays.asList(new Usuario("Luis","Mosquera","luchitomosquera@mail.com"),
				new Usuario("Santiago","Herrera","santiagoherrera@mail.com"),
				new Usuario("Franco","Armani","francoarmanin@mail.com"),
				new Usuario("Pedro","Nel","pedronel@mail.com"));
		usuarios.add(new Usuario("Luis","Mosquera","luchitomosquera@mail.com"));
		usuarios.add(new Usuario("Santiago","Herrera","santiagoherrera@mail.com"));
		usuarios.add(new Usuario("Franco","Armani","francoarmanin@mail.com"));*/
		
		model.addAttribute("titulo", textoListar);
		return "listar";
	}
	
	@ModelAttribute("usuarios")
	public List<Usuario> poblarusuarios(){
		List<Usuario> usuarios = Arrays.asList(new Usuario("Luis","Mosquera","luchitomosquera@mail.com"),
				new Usuario("Santiago","Herrera","santiagoherrera@mail.com"),
				new Usuario("Franco","Armani","francoarmanin@mail.com"),
				new Usuario("Pedro","Nel","pedronel@mail.com"));
		return usuarios;
		
	}

}
