package com.hosty.spring.boot.web.app.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/params")
public class EjemploParamsController {
	
	@GetMapping("/")
	public String index() {
		return "params/index";
	}
	
	@GetMapping("/string")
	public String param(@RequestParam(name="texto", required = false) String texto ,Model model) {
		model.addAttribute("resultado", "El texto enviado a convertir a minuzcula es: "+ texto.toLowerCase());
		return "params/ver";
	}
	
	@GetMapping("/mixParams")
	public String param(@RequestParam String saludo ,@RequestParam Integer numero, Model model) {
		model.addAttribute("resultado", "El saludo enviado es: '"+ saludo +"' y el número es '" + numero + "'");
		return "params/ver";
	}
	
	@GetMapping("/mixParamsRequest")
	public String param(HttpServletRequest req, Model model) {
		String saludo = req.getParameter("saludo");
		Integer numero = null; 
		try {
			numero = Integer.parseInt(req.getParameter("numero"));
		} catch (NumberFormatException e) {
			numero = 0;
		}
		
		model.addAttribute("resultado", "El saludo enviado es: '"+ saludo +"' y el número es '" + numero + "'");
		return "params/ver";
	}

}
