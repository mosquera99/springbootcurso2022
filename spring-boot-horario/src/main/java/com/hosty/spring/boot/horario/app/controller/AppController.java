package com.hosty.spring.boot.horario.app.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AppController {
	
	@Value("${config.horario.apertura}")
	private Integer apertura;

	@Value("${config.horario.cierre}")
	private Integer cierre;
	
	@GetMapping({"","/","/home"})
	public String index(Model md) {
		md.addAttribute("title", "Bienvenido al horario de atencion a clientes");
		return "index";
	}
	
	@GetMapping("/cerrado")
	public String cerrado(Model md) {
		StringBuilder sb = new StringBuilder("Cerrado, por favor visitenos desde las ");
		sb.append(apertura);
		sb.append(" y las ");
		sb.append(cierre);
		sb.append(" hrs. Gracias. ");
		
		md.addAttribute("title", "Fuera del horario de atención");
		md.addAttribute("horario", sb);
		return "cerrado";
	}

}
