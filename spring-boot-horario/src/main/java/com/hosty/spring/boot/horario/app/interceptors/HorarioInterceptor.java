package com.hosty.spring.boot.horario.app.interceptors;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component("horarioInterceptor")
public class HorarioInterceptor implements HandlerInterceptor {

	@Value("${config.horario.apertura}")
	private Integer apertura;

	@Value("${config.horario.cierre}")
	private Integer cierre;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		Calendar calendar = Calendar.getInstance();
		int horaActual = calendar.get(Calendar.HOUR_OF_DAY);
		if (horaActual >= apertura && horaActual < cierre) {
			StringBuilder sb = new StringBuilder("Bienvendios al horario de atencion clientes");
			sb.append(" atendemos desde las ");
			sb.append(apertura);
			sb.append("hrs. ");
			sb.append("hasta las ");
			sb.append(cierre);
			sb.append("hrs. ");
			sb.append("Gracias por su visita. ");
			request.setAttribute("mensaje", sb.toString());
			return true;
		}
		
		response.sendRedirect(request.getContextPath().concat("/cerrado"));
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String mensaje = (String) request.getAttribute("mensaje");
		modelAndView.addObject("horario", mensaje);
	}

}
