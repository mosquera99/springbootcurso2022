package com.hosty.spring.boot.error.app.controllers;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.hosty.spring.boot.error.app.errors.UsuarioNoEncontradoException;

@ControllerAdvice
public class ErrorHandlerController {
	
	@ExceptionHandler(ArithmeticException.class)
	public String aritmeticaError(Exception ex, Model md) {
		md.addAttribute("error", "Error de aritmética");
		md.addAttribute("message", ex.getMessage());
		md.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
		md.addAttribute("timestamp", new Date());
		return "error/generica";
	}
	
	@ExceptionHandler(NumberFormatException.class)
	public String numberError(NumberFormatException ex, Model md) {
		md.addAttribute("error", "Error: Formato de numero invalido!!");
		md.addAttribute("message", ex.getMessage());
		md.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
		md.addAttribute("timestamp", new Date());
		return "error/generica";
	}
	
	@ExceptionHandler(UsuarioNoEncontradoException.class)
	public String usuarioNoEncontrado(UsuarioNoEncontradoException ex, Model md) {
		md.addAttribute("error", "Error: Usuario no existe!!");
		md.addAttribute("message", ex.getMessage());
		md.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR.value());
		md.addAttribute("timestamp", new Date());
		return "error/generica";
	}


}
