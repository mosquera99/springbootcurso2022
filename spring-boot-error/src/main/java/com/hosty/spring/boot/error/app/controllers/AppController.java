package com.hosty.spring.boot.error.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hosty.spring.boot.error.app.errors.UsuarioNoEncontradoException;
import com.hosty.spring.boot.error.app.models.domain.Usuario;
import com.hosty.spring.boot.error.app.service.IUsuarioService;

@Controller
public class AppController {
	
	@Autowired
	private IUsuarioService service;
	
	@GetMapping("/index")
	public String index(){
		int valor= 100/0;
//		Integer valor = Integer.parseInt("10ED");
		return "index";
	}
	
	@GetMapping("/ver/{id}")
	public String ver(@PathVariable Integer id, Model md) {
//		if (null==usu) {
//			throw new UsuarioNoEncontradoException(id.toString());
//		}
		
		Usuario usu = service.usuarioXIdOptional(id).orElseThrow(() -> new UsuarioNoEncontradoException(id.toString()));

		md.addAttribute("usuaio", usu);
		md.addAttribute("title", "Detalle usuario: ".concat(usu.getNombre()));

		return "ver";
	}

}
