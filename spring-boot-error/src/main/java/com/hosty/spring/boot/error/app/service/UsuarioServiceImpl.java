package com.hosty.spring.boot.error.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.hosty.spring.boot.error.app.models.domain.Usuario;

@Service
public class UsuarioServiceImpl implements IUsuarioService {
	
	private List<Usuario> usuarios;
	
	
	public UsuarioServiceImpl() {
		usuarios = new ArrayList<>();
		usuarios.add(new Usuario(1, "Santiago","Herrera"));
		usuarios.add(new Usuario(2, "Pedro","Martinez"));
		usuarios.add(new Usuario(3, "Santiago","Suarez"));
		usuarios.add(new Usuario(4, "Andes","Guzman"));
		usuarios.add(new Usuario(5, "Pepa","Garcia"));
		usuarios.add(new Usuario(6, "Luis","Perez"));
	}
	
	@Override
	public Usuario usuarioXId(Integer id) {
		Usuario resultado = null;
		for (Usuario usu : usuarios) {
			if (id.equals(usu.getId())) {
				resultado=usu; 
				break;
			}
		}
		return resultado;
	}


	@Override
	public Optional<Usuario> usuarioXIdOptional(Integer id) {
		Usuario usu = usuarioXId(id);
		return Optional.ofNullable(usu);
	}
	
	

}
