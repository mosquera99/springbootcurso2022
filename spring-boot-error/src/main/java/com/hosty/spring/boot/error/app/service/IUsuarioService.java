package com.hosty.spring.boot.error.app.service;

import java.util.Optional;

import com.hosty.spring.boot.error.app.models.domain.Usuario;

public interface IUsuarioService {
	
	Usuario usuarioXId(Integer id);
	Optional<Usuario> usuarioXIdOptional(Integer id);

	
}
